*PADS-LIBRARY-PART-TYPES-V9*

$GND_SYMS PCBNULL I UND 0 0 0 0 2
TIMESTAMP 2015.02.22.17.07.16
OFF 3
GNDT U GND
GNDE U GNDE
GNDC U GNDC

$OSR_SYMS PCBNULL I UND 0 0 0 0 2
TIMESTAMP 2015.02.20.18.38.18
OFF 2
$OSR_BI_LEFT U 
$OSR_BI_RIGHT U 

$PWR_SYMS PCBNULL I UND 0 0 0 0 2
TIMESTAMP 2015.02.22.17.07.34
OFF 2
VCC U 
VCC- U 

RES-VAR PCBNULL I RES 5 1 0 0 0
TIMESTAMP 2021.01.01.16.59.47
"DESCRIPTION" Varibable Resistor
"Geometry.Height" 1mm
"ROHS" Yes
"TOLERANCE" 1%
"VALUE" 1k
GATE 4 3 0
RESVAR-H1
RESVAR-V1
RESVAR-H2
RESVAR-V2
1 0 U
2 0 U
3 0 U

RES-VAR_1k2 PCBNULL I RES 5 1 0 0 0
TIMESTAMP 2021.01.01.16.59.47
"DESCRIPTION" Varibable Resistor
"Geometry.Height" 1mm
"ROHS" Yes
"TOLERANCE" 1%
"VALUE" 1k2
GATE 4 3 0
RESVAR-H1
RESVAR-V1
RESVAR-H2
RESVAR-V2
1 0 U
2 0 U
3 0 U


*END*
